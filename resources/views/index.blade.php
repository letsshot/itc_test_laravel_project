<!DOCTYPE html>
<html>
    <head>
        <title>Homepage</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>
        <div class="container">

            <div class="content">
                <div class="title">ITC Test</div>
            </div>

            <div class="form">
                 <input id="file_upload" name="file_upload" type="file" multiple="true">
                 <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-2.2.4.min.js"></script> 
                 <script src="{{asset('resources/org/uploadify/jquery.uploadify.min.js')}}" type="text/javascript"></script>
                 <script type="text/javascript" src="{{asset('resources/org/layer/layer.js')}}" ></script>
                <link rel="stylesheet" type="text/css" href="{{asset('resources/org/uploadify/uploadify.css')}}">
                <script type="text/javascript">
                <?php $timestamp = time();?>
                $(function() {
                $('#file_upload').uploadify({
                    'formData'     : {
                        'timestamp' : '<?php echo $timestamp;?>',
                        '_token'     : "{{csrf_token()}}"
                    },
                    'swf'      : "{{asset('resources/org/uploadify/uploadify.swf')}}",
                    'uploader' : "{{url('upload')}}"
                    });
                 });
                 </script>
                <style>
                .uploadify{display:inline-block;}
                .uploadify-button{border:none; border-radius:5px; margin-top:8px;}
                table.add_tab tr td span.uploadify-button-text{color: #FFF; margin:0;}
                </style>
            </div>


            <div class="show">
                <form action="#" method="post">

                    <div class="create href">
                        <a href="{{url('index/create')}}">Create</a>
                    </div>
                    <div class="result_wrap">
                        <div class="result_content">
                             <table class="list_tab">
                                <tr>
                                    <th class="tc" width="5%">ID</th>
                                    <th class="tc" width="5%">SKU</th>
                                    <th class="tc" width="5%">EAN</th>
                                    <th class="tc" width="5%">NAME</th>
                                    <th class="tc" width="5%">STOCK</th>
                                    <th class="tc" width="5%">AVAILABILITY</th>
                                 </tr>

                                @foreach($data as $product)
                                <tr>
                                    <td class="tc">{{$product->id}}</td>
                                    <td class="tc">{{$product->sku}}</td>
                                    <td class="tc">{{$product->ean}}</td>
                                    <td class="tc">{{$product->name}}</td>
                                    <td class="tc">{{$product->stock}}</td>
                                    <td class="tc">{{$product->availability}}</td>

                                    <td>
                                        <a href="{{url('index/'.$product->id.'/edit')}}">Edit</a>
                                        <a href="javascript:;" onclick="delCate({{$product->id}})">Delete</a>
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </form>

                 <script>
                    //Delete
                    function delCate(id) {
                        layer.confirm('Do you want to delete', {
                            btn: ['Confirm','Cancel'] //Button
                        }, function(){
                            $.post("{{url('index')}}/"+id,{'_method':'delete','_token':"{{csrf_token()}}"},function (data) {
                                if(data.status==0){
                                    location.href = location.href;
                                    layer.msg(data.msg, {icon: 6});
                                }else{
                                    layer.msg(data.msg, {icon: 5});
                                }
                            });
                        }, function(){

                        });
                    }
                </script>
            </div>       
        </div>
    </body>
</html>
