<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<script type="text/javascript" src="{{asset('resources/org/layer/layer.js')}}"></script>
	<script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-2.2.4.min.js"></script>
</head>
<body>
	<div class="result_wrap">
	    <form action="{{url('index/')}}" method="post">
	        {{csrf_field()}}
	        <table class="add_tab">
	            <tr>
	                <th>ID：</th>
	                <td>
	                    <input type="text" name="id">
	                </td>
	            </tr>
	            <tr>
	                <th>SKU：</th>
	                <td>
	                    <input type="text" name="sku">
	                </td>
	            </tr>
	            <tr>
	                <th>EAN：</th>
	                <td>
	                    <input type="text" name="ean">
	                </td>
	            </tr>
	            <tr>
	                <th>NAME：</th>
	                <td>
	                    <input type="text" name="name">
	                </td>
	            </tr>
	            <tr>
	                <th>STOCK：</th>
	                <td>
	                    <input type="text" name="stock">
	                </td>
	            </tr>
	           	<tr>
	                <th>AVAILABILITY：</th>
	                <td>
	                    <input type="text" name="availability">
	                </td>
	            </tr>
	            <tr>
	                <th></th>
	                <td>
	                    <input type="submit" value="submit">
	                    <input type="button" class="back" onclick="history.go(-1)" value="Back">
	                </td>
	            </tr>
	        
	        </table>
	    </form>
	</div>
</body>
</html>