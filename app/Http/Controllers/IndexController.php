<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use App\Http\Model\Product;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class IndexController extends Controller{
	public function index()
	{
		$products = Product::get();
		return view('index')->with('data',$products);
	}

	public function create()
    {
        $data = Product::where('id',0)->get();
        return view('add',compact('data'));
    }

    public function store()
    {
        $input = Input::except('_token');
        $rules = [
            'id'=>'required',
        ];

        $message = [
            'id.required'=>'Id cant be Null',
        ];

        $validator = Validator::make($input,$rules,$message);

        if($validator->passes()){
            $re = Product::create($input);
            if($re){
                return redirect('index');
            }else{
                return back()->with('errors','Create fail, try again');
            }
        }else{
            return back()->withErrors($validator);
        }
    }

    public function show()
    {

    }

	public function edit($id)
    {
        $field = Product::find($id);
        $data = Product::where('id',0)->get();
        return view('edit',compact('field','data'));
    }

    public function update($id)
    {
        $input = Input::except('_token','_method');
        $re = Product::where('id',$id)->update($input);
        if($re){
            return redirect('index');
        }else{
            return back()->with('errors','Update fail, try again！');
        }
    }

	public function destroy($id)
    {
        $re = Product::where('id',$id)->delete();

        if($re){
            $data = [
                'status' => 0,
                'msg' => 'Delete Success',
            ];
        }else{
            $data = [
                'status' => 1,
                'msg' => 'Delete Fail, try agin later.',
            ];
        }
        return $data;
    }
}