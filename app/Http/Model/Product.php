<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table="product";
    protected $fillable =  ['id', 'sku', 'ean', 'name', 'stock', 'availability'];
    public $timestamps=false;
}
